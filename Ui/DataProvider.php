<?php

namespace Vetal\Brands\Ui;

use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class DataProvider
 */
class DataProvider extends AbstractDataProvider
{
    protected $collection;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        $collectionFactory,
        array $meta = [],
        array $data = [])
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        $result = [];
        foreach ($this->collection->getItems() as $item) {
            $_data = $item->getData();
            if (isset($_data['image'])) {
                $image = [];
                $image[0]['name'] = $item->getImage();
                $image[0]['url'] = $item->getImageUrl();
                $_data['image'] = $image;
            }
            $item->setData($_data);
            $result[$item->getId()]['general'] = $_data;

        }
        return $result;
    }
}
