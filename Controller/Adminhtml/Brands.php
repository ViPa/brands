<?php

namespace Vetal\Brands\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Vetal\Brands\Api\BrandsRepositoryInterface;

/**
 * Abstract class Brands to extend in backend actions
 */
abstract class Brands extends Action
{

    /**
     * brands factory
     *
     * @var BrandsRepositoryInterface
     */
    protected $brandsRepository;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Registry $registry
     * @param BrandsRepositoryInterface $brandsRepository
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        Registry                  $registry,
        BrandsRepositoryInterface $brandsRepository,
        PageFactory               $resultPageFactory,
        Context                   $context
    )
    {
        $this->coreRegistry = $registry;
        $this->brandsRepository = $brandsRepository;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

}
