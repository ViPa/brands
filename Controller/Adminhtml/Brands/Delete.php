<?php

namespace Vetal\Brands\Controller\Adminhtml\Brands;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Vetal\Brands\Model\BrandsFactory;

/**
 * Class Delete - implement delete brand action in action's column
 */
class Delete extends Action
{
    public $brandsFactory;

    /**
     * @param Context $context
     * @param BrandsFactory $brandsFactory
     */
    public function __construct(
        Context       $context,
        BrandsFactory $brandsFactory
    )
    {
        $this->brandsFactory = $brandsFactory;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        try {
            $brandsModel = $this->brandsFactory->create();
            $brandsModel->load($id);
            $brandsModel->delete();
            $this->messageManager->addSuccessMessage(__('You deleted the brand.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Vetal_Brands::manage');
    }
}
