<?php

namespace Vetal\Brands\Controller\Adminhtml\Brands;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Vetal\Brands\Model\BrandsFactory;
use Vetal\Brands\Model\ImageUploader;

/**
 * Class Save - implements saving brand data
 */
class Save extends Action
{
    private $brandsFactory;
    private $imageUploader;

    /**
     * @param Context $context
     * @param BrandsFactory $brandsFactory
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context       $context,
        BrandsFactory $brandsFactory,
        ImageUploader $imageUploader
    )
    {
        $this->imageUploader = $imageUploader;
        $this->brandsFactory = $brandsFactory;
        parent::__construct($context);
    }

    /**
     * run the action
     *
     * @return Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue()['general'];

        if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
            $this->imageUploader->moveFileFromTmp($data['image']);
        } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = ImageUploader::NO_LOGO_IMAGE;
        }
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            $this->brandsFactory->create()
                ->setData($data)
                ->save();
            $this->messageManager->addSuccessMessage(__('You saved the brand'));
            $resultRedirect->setPath('vetal_brands/brands/index');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the brand'));
            $resultRedirect->setPath('vetal_brands/brands/index');
        }

        return $resultRedirect;
    }
}
