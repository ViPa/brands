<?php

namespace Vetal\Brands\Controller\Adminhtml\Brands;

use Vetal\Brands\Controller\Adminhtml\Brands;
use Vetal\Brands\Controller\RegistryConstants;

/**
 * Class Edit - implements edit brand actions in the action's column
 */
class Edit extends Brands
{
    /**
     * Initialize current brand and set it in the registry.
     *
     * @return int
     */
    protected function _initBrand()
    {
        $brandId = $this->getRequest()->getParam('id');
        $this->coreRegistry->register(RegistryConstants::CURRENT_BRAND_ID, $brandId);

        return $brandId;
    }

    /**
     * Edit or create Brand
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $brandId = $this->_initBrand();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Vetal_Brands::manage_brands');
        $resultPage->getConfig()->getTitle()->prepend(__('Brands'));
        $resultPage->addBreadcrumb(__('News'), __('News'));
        $resultPage->addBreadcrumb(__('Brands'), __('Brands'), $this->getUrl('vetal_brands/brands'));

        if ($brandId === null) {
            $resultPage->addBreadcrumb(__('New Brand'), __('New Brand'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Brand'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Brand'), __('Edit Brand'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->brandsRepository->getById($brandId)->getTitle()
            );
        }
        return $resultPage;
    }

}

