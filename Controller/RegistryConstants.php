<?php

namespace Vetal\Brands\Controller;

class RegistryConstants
{
    /**
     * Registry key where current brand ID is stored
     */
    const CURRENT_BRAND_ID = 'current_brand_id';
}
