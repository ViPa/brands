<?php

namespace Vetal\Brands\Model\Attribute\Frontend;

use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Framework\DataObject;

class Brands extends AbstractFrontend
{
    /**
     * @param DataObject $object
     * @return bool|mixed
     */
    public function getValue(DataObject $object)
    {
        $value = $object->getData($this->getAttribute()->getAttributeCode());
        return $value;
    }
}
