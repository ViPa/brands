<?php

namespace Vetal\Brands\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use Vetal\Brands\Model\ResourceModel\Brands\Collection;
use Vetal\Brands\Model\ResourceModel\Brands\CollectionFactory;

/**
 * Class Brands - used for generating options -
 * sources for array of attribute values
 */
class Brands extends AbstractSource
{
    protected CollectionFactory $collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return Collection
     */
    public function getAllBrands() : Collection
    {
        return $this->collectionFactory->create();
    }

    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            foreach ($this->getAllBrands() as $brand)
            {
                $this->_options[] =  ['label' => __($brand->getTitle()), 'value' => $brand->getId()];
            }
        }

        return $this->_options;
    }
}
