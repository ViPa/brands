<?php
namespace Vetal\Brands\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Vetal\Brands\Api\Data\BrandsInterface;
use Vetal\Brands\Model\ResourceModel\Brands as ResourceModel;
use Vetal\Brands\Model\ImageUploader;

/**
 * Class Brands implements BrandsInterface
 */
class Brands extends AbstractModel implements
    BrandsInterface,
    IdentityInterface
{
    const CACHE_TAG = 'brands_table';
    protected $_cacheTag = 'brands_table';
    protected $_eventPrefix = 'brands_table';

    /**
     * Image uploader
     *
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @param \Vetal\Brands\Model\ImageUploader $imageUploader
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        ImageUploader $imageUploader,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->imageUploader = $imageUploader;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues(): array
    {
        $values = [];

        return $values;
    }

    /**
     * Get Brand ID
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->getData('id');
    }

    /**
     * Set Brand ID
     * @param $id
     * @return mixed|void
     */
    public function setId($id)
    {
        $this->setData('id', $id);
    }

    /**
     * Get Brand title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->getData('title');
    }

    /**
     * Set Brand title
     *
     * @param string $title
     * @return mixed|void
     */
    public function setTitle(string $title)
    {
        $this->setData('title', $title);
    }

    /**
     * Get Brand description
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getData('description');
    }

    /**
     * Set Brand description
     *
     * @param string $description
     * @return mixed|void
     */
    public function setDescription(string $description)
    {
        $this->setData('description', $description);
    }

    /**
     * Get Brand image filename
     *
     * @return string
     */
    public function getImage(): string
    {
        return $this->getData('image');
    }

    /**
     * Set Brand image filename
     *
     * @param string $image
     * @return mixed|void
     */
    public function setImage(string $image)
    {
        $this->setData('image', $image);
    }

    /**
     * Get Brand image filename URL
     *
     * @return bool|string
     * @throws LocalizedException
     */
    public function getImageUrl()
    {
        $url = false;
        $image = $this->getImage();
        if ($image) {
            if (is_string($image)) {
                $uploader = $this->imageUploader;
                $url = $uploader->getBaseUrl().$uploader->getBasePath().$image;
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the brand image url.')
                );
            }
        }
        return $url;
    }
}
