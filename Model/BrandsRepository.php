<?php

namespace Vetal\Brands\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

use Vetal\Brands\Api\BrandsRepositoryInterface;
use Vetal\Brands\Api\Data\BrandsInterface;
use Vetal\Brands\Model\BrandsFactory;
use Vetal\Brands\Model\ResourceModel\Brands as ObjectResourceModel;
use Vetal\Brands\Model\ResourceModel\Brands\CollectionFactory;

/**
 * Class BrandsRepository
 */
class BrandsRepository implements BrandsRepositoryInterface
{
    protected $objectFactory;
    protected $objectResourceModel;
    protected $collectionFactory;
    protected $searchResultsFactory;

    /**
     * BrandsRepository constructor.
     *
     * @param BrandsFactory $objectFactory
     * @param ObjectResourceModel $objectResourceModel
     * @param CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        BrandsFactory                 $objectFactory,
        ObjectResourceModel           $objectResourceModel,
        CollectionFactory             $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory
    )
    {
        $this->objectFactory = $objectFactory;
        $this->objectResourceModel = $objectResourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Create or update a Brand.
     *
     * @param BrandsInterface $object
     * @return BrandsInterface
     * @throws CouldNotSaveException
     */
    public function save(BrandsInterface $object)
    {
        try {
            $this->objectResourceModel->save($object);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $object;
    }

    /**
     * Get a Brand by Id
     *
     * @param int $id
     * @return BrandsInterface
     * @throws NoSuchEntityException If Brand with the specified ID does not exist.
     */
    public function getById(int $id)
    {
        $object = $this->objectFactory->create();
        $this->objectResourceModel->load($object, $id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }
        return $object;
    }

    /**
     * Delete a Brand
     *
     * @param BrandsInterface $brand
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(BrandsInterface $brand)
    {
        try {
            $this->objectResourceModel->delete($brand);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete a Brand by Id
     *
     * @param int $id
     * @return bool|BrandsInterface
     */
    public function deleteById(int $id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * Return list of Brands
     *
     * @return Vetal\Brands\Api\Data\BrandsInterface[]
     */
    public function getList()
    {
        return $this->collectionFactory->create()->getItems();
    }

    /**
     * Return list of Brands using SearchCriteria interface
     *
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getListUsingCriteria(SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $collection = $this->collectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $objects = [];
        foreach ($collection as $objectModel) {
            $objects[] = $objectModel;
        }
        $searchResults->setItems($objects);
        return $searchResults;
    }
}
