<?php

namespace Vetal\Brands\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Brands
 */
class Brands extends AbstractDb
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('brands_table', 'id');
    }
}
