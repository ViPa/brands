<?php
namespace Vetal\Brands\Model\ResourceModel\Brands;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Vetal\Brands\Model\Brands as Model;
use Vetal\Brands\Model\ResourceModel\Brands as ResourceModel;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'vetal_brands_collection';
    protected $_eventObject = 'brands_collection';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}

