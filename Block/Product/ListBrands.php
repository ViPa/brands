<?php
namespace Vetal\Brands\Block\Product;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Helper\Output;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;
use Vetal\Brands\Api\BrandsRepositoryInterface;

/**
 * Class ListBrands extends core ListProduct class
 * to add Brand image for every product
 * in Product List Page
 */
class ListBrands extends ListProduct
{
    protected BrandsRepositoryInterface $brandsRepository;

    /**
     * @param BrandsRepositoryInterface $brandsRepository
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param array $data
     * @param Output|null $outputHelper
     */
    public function __construct(
        BrandsRepositoryInterface   $brandsRepository,
        Context                     $context,
        PostHelper                  $postDataHelper,
        Resolver                    $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data                        $urlHelper,
        array                       $data = [],
        ?Output                     $outputHelper = null
    )
    {
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data, $outputHelper);
        $this->brandsRepository = $brandsRepository;
    }

    /**
     * Get URL of the Brand image if file exist or return false
     *
     * @param int $brandId
     * @return bool|string
     */
    public function getBrandImageByProduct(int $brandId)
    {
         return $this->brandsRepository->getById($brandId)->getImageUrl();
    }
}
