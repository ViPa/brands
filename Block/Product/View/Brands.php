<?php

namespace Vetal\Brands\Block\Product\View;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Vetal\Brands\Api\BrandsRepositoryInterface;
use Vetal\Brands\Model\ResourceModel\Brands\CollectionFactory;

/**
 * Class Brands used to add Brand image
 * to the Product Detailed Page
 */
class Brands extends Template
{
    protected CollectionFactory $collectionFactory;
    private BrandsRepositoryInterface $brandsRepository;
    protected Registry $_registry;

    /**
     * @param Template\Context $context
     * @param Registry $registry
     * @param CollectionFactory $collectionFactory
     * @param BrandsRepositoryInterface $brandsRepository
     * @param array $data
     */
    public function __construct(
        Template\Context          $context,
        Registry                  $registry,
        CollectionFactory         $collectionFactory,
        BrandsRepositoryInterface $brandsRepository,
        array                     $data = []
    )
    {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->brandsRepository = $brandsRepository;
        $this->_registry = $registry;
    }

    /**
     * @return mixed
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * Get current product from registry
     *
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * Get URL of the Brand image for current product
     * if file exist or return false
     *
     * @return bool|string
     */
    public function getBrandImageByProduct()
    {
        $product = $this->getCurrentProduct();
        $brandId = $product->getCustomAttribute('product_brand_attribute')->getValue();

        return $this->brandsRepository->getById($brandId)->getImageUrl();
    }

}
