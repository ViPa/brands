<?php

namespace Vetal\Brands\Block;

use Magento\Framework\View\Element\Template;
use Vetal\Brands\Api\BrandsRepositoryInterface;
use Vetal\Brands\Model\ResourceModel\Brands\Collection;
use Vetal\Brands\Model\ResourceModel\Brands\CollectionFactory;

/**
 * Class Brands used for listing Brands on frontend
 */
class Brands extends Template
{
    protected CollectionFactory $collectionFactory;
    private BrandsRepositoryInterface $brandsRepository;

    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        BrandsRepositoryInterface $brandsRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->brandsRepository = $brandsRepository;
    }

    /**
     * Get list of all Brands
     *
     * @return Collection
     */
    public function getAllBrands(): Collection
    {
        return $this->collectionFactory->create();
    }

}
