<?php

namespace Vetal\Brands\Block\Adminhtml\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Back button class
 */
class Back extends Generic implements ButtonProviderInterface
{
    /**
     * Get button options
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10,
        ];
    }

    /**
     * BackURL for Back button
     *
     * @return mixed
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }
}
