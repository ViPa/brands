<?php
namespace Vetal\Brands\Api\Data;

/**
 * Interface BrandsInterface
 */
interface BrandsInterface
{
    /**
     * Get Brand ID
     *
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Set Brand ID
     * @param $id
     * @return mixed|void
     */
    public function setId($id);

    /**
     * Get Brand title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Set Brand title
     *
     * @param string $title
     * @return mixed|void
     */
    public function setTitle(string $title);

    /**
     * Get Brand description
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Brand description
     *
     * @param string $description
     * @return mixed|void
     */
    public function setDescription(string $description);

    /**
     * Get Brand image filename
     *
     * @return string
     */
    public function getImage(): string;

    /**
     * Get Brand image filename URL
     *
     * @return bool|string
     * @throws LocalizedException
     */
    public function getImageUrl();

    /**
     * Set Brand image filename
     *
     * @param string $image
     * @return mixed|void
     */
    public function setImage(string $image);
}
