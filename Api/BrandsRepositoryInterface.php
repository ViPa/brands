<?php

namespace Vetal\Brands\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Vetal\Brands\Api\Data\BrandsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface BrandsRepositoryInterface
 */
interface BrandsRepositoryInterface
{
    /**
     * Create or update a Brand.
     *
     * @param BrandsInterface $brand
     * @return BrandsInterface
     * @throws CouldNotSaveException
     */
    public function save(BrandsInterface $brand);

    /**
     * Get a Brand by Id
     *
     * @param int $id
     * @return BrandsInterface
     * @throws NoSuchEntityException If Brand with the specified ID does not exist.
     */
    public function getById(int $id);

    /**
     * Return list of Brands
     *
     * @return Vetal\Brands\Api\Data\BrandsInterface[]
     */
    public function getList();

    /**
     * Retrieve Brands which match a specified criteria.
     *
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getListUsingCriteria(SearchCriteriaInterface $criteria);

    /**
     * Delete a Brand
     *
     * @param BrandsInterface $brand
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(BrandsInterface $brand);

    /**
     * Delete a Brand by Id
     *
     * @param int $id
     * @return bool|BrandsInterface
     */
    public function deleteById(int $id);
}
